Assumptions:
 1. I have used an in memory map to represent storage, typically this would be replaced with a OfferRepository that access a database
 2. There is limited error handling, the application does some validation but the error responses will just be the exception messages
 3. Offer has a "relatedProductId", which is just an int. It is to represent the id of the product the offer is associated with. I have assumed it is OK not to add Product to this application for now

End points will be:
localhost:8085/offer?id=1 (Http method Get/Delete)
localhost:8085/offer/all
localhost:8085/offer (Http method Post/Put)
