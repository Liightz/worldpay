package application.validator;

import application.domain.Offer;
import application.exception.ValidationException;
import org.junit.Before;
import org.junit.Test;

public class OfferValidatorTest {

    private OfferValidator offerValidator;
    private Offer VALID_OFFER;

    @Before
    public void setup() {
        this.offerValidator = new OfferValidator();
        VALID_OFFER = new Offer();
        VALID_OFFER.setDescription("Customer friendly description");
        VALID_OFFER.setRelatedProductId(1);
        VALID_OFFER.setOfferPrice(1.99);
    }

    @Test
    public void shouldNotThrowExceptionForValidOffer() throws ValidationException {
        offerValidator.validate(VALID_OFFER);
    }

    @Test(expected = ValidationException.class)
    public void shouldThrowValidationExceptionWhenDescriptionIsNull() throws ValidationException {
        VALID_OFFER.setDescription(null);
        offerValidator.validate(VALID_OFFER);
    }

    @Test(expected = ValidationException.class)
    public void shouldThrowValidationExceptionWhenDescriptionIsEmpty() throws ValidationException {
        VALID_OFFER.setDescription("");
        offerValidator.validate(VALID_OFFER);
    }

    @Test(expected = ValidationException.class)
    public void shouldThrowValidationExceptionWhenOfferHasNoRelatedProductId() throws ValidationException {
        VALID_OFFER.setRelatedProductId(0);
        offerValidator.validate(VALID_OFFER);
    }

    @Test(expected = ValidationException.class)
    public void shouldThrowValidationExceptionWhenPriceIsToMoreThan2DecimalPlaces() throws ValidationException {
        VALID_OFFER.setOfferPrice(1.999);
        offerValidator.validate(VALID_OFFER);
    }
}
