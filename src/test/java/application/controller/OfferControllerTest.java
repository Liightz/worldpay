package application.controller;

import application.domain.Offer;
import application.exception.ValidationException;
import application.service.OfferService;
import application.validator.OfferValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class OfferControllerTest {

    private static final ParameterizedTypeReference<Offer> responseType = new ParameterizedTypeReference<Offer>() {};

    @LocalServerPort
    private int serverPort;

    @MockBean
    private OfferService offerService;

    private String BASE_URL;
    private Offer existingOffer;

    @Before
    public void setup() {
        BASE_URL = "http://localhost:" + this.serverPort + "/offer";
        existingOffer = instantiateOffer(1, "Description", 2.99, 1);
    }

    @Test
    public void shouldReturnOkOffersWhenGettingAllOffers() {
        when(offerService.getAllOffers()).thenReturn(Arrays.asList(existingOffer));
        String url = BASE_URL+"/all";
        ResponseEntity<Offer[]> entity = new TestRestTemplate().getForEntity(url, Offer[].class);
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        List<Offer> offers = Arrays.asList(entity.getBody());
        assertEquals(offers.size(), 1);
    }

    @Test
    public void shouldReturnOfferWhenFindingById() throws ValidationException {
        when(offerService.getOffer(1)).thenReturn(existingOffer);
        ResponseEntity<Offer> entity = new TestRestTemplate().getForEntity(
                BASE_URL+"?id=1", Offer.class);
        Offer offer = entity.getBody();
        assertEquals(offer, existingOffer);
    }

    @Test
    public void shouldCreateOffer() throws ValidationException {
        Offer offer = instantiateOffer(null, "Description", 2.99, 1);
        Offer expectedResult = new Offer(offer);
        expectedResult.setId(2);
        when(offerService.addOffer(offer)).thenReturn(expectedResult);
        ResponseEntity<Offer> entity = new TestRestTemplate().postForEntity (
                 BASE_URL, offer, Offer.class);
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        Offer created = entity.getBody();
        assertEquals(created, expectedResult);
    }

    @Test
    public void shouldUpdateOffer() throws ValidationException {
        Offer offer = instantiateOffer(2, "Description", 2.99, 1);
        when(offerService.updateOffer(offer)).thenReturn(offer);
        HttpEntity<Offer> requestEntity = new HttpEntity<Offer>(offer);
        ResponseEntity<Offer> entity = new TestRestTemplate().exchange(
                BASE_URL, HttpMethod.PUT, requestEntity, Offer.class);
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        Offer updated = entity.getBody();
        assertEquals(updated, offer);
    }

    @Test
    public void shouldDeleteOffer() throws ValidationException {
        doNothing().when(offerService).deleteOffer(1);
        ResponseEntity<Void> entity = new TestRestTemplate().exchange(
                BASE_URL+"?id=1", HttpMethod.DELETE, null, Void.class);
        assertEquals(HttpStatus.OK, entity.getStatusCode());
    }

    private Offer instantiateOffer(Integer id, String description, double price, int relatedProductId) {
        Offer offer = new Offer();
        offer.setId(id);
        offer.setDescription(description);
        offer.setOfferPrice(price);
        offer.setRelatedProductId(relatedProductId);
        return offer;
    }

}
