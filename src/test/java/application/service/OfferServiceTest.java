package application.service;

import application.domain.Offer;
import application.exception.ValidationException;
import application.validator.OfferValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OfferServiceTest {

    private static final int NON_EXISTENT_ID = 100;

    @Mock
    private OfferValidator validator;

    private OfferService offerService;

    private Offer EXISTING_OFFER;

    @Before
    public void setup() throws ValidationException {
        this.offerService = new OfferService(validator);
        doNothing().when(validator).validate(any(Offer.class));
        Offer offer = instantiateOffer("First offer", 5.99, 1);
        EXISTING_OFFER = offerService.addOffer(offer);
        reset(validator);
    }

    @Test
    public void shouldBeAbleToAddAnOffer() throws ValidationException {
        Offer offer = instantiateOffer("New offer", 1.99, 4);
        Offer saved = offerService.addOffer(offer);
        assertNotNull(saved.getId());
    }

    @Test(expected = ValidationException.class)
    public void shouldThrowExceptionWhenTryingToAddOfferWithAnId() throws ValidationException {
        Offer offer = instantiateOffer("New offer", 1.99, 4);
        offer.setId(4);
        Offer saved = offerService.addOffer(offer);
    }

    @Test
    public void shouldValidateOfferBeforeAddingIt() throws ValidationException {
        Offer offer = instantiateOffer("New offer", 1.99, 4);
        Offer saved = offerService.addOffer(offer);
        verify(validator).validate(any(Offer.class));
    }

    @Test
    public void shouldBeAbleToDeleteAnOffer() throws ValidationException {
        offerService.deleteOffer(EXISTING_OFFER.getId());
        List<Offer> offers = offerService.getAllOffers();
        assertEquals(offers.size(), 0);
    }

    @Test(expected = ValidationException.class)
    public void shouldThrowExceptionIfTryingToDeleteAnOfferThatDoesntExist() throws ValidationException {
        offerService.deleteOffer(NON_EXISTENT_ID);
        List<Offer> offers = offerService.getAllOffers();
    }

    @Test
    public void shouldBeAbleToRetrieveOfferById() throws ValidationException {
        Offer offer = offerService.getOffer(EXISTING_OFFER.getId());
        assertEquals(offer, EXISTING_OFFER);
    }

    @Test(expected = ValidationException.class)
    public void shouldThrowExceptionWhenTryingToRetrieveOfferThatDoesntExist() throws ValidationException {
        Offer offer = offerService.getOffer(NON_EXISTENT_ID);
    }

    @Test
    public void shouldBeAbleToRetrieveAllOffers() {
        List<Offer> offers = offerService.getAllOffers();
        assertEquals(offers.size(), 1);
    }

    @Test
    public void shouldBeAbleToUpdateOffer() throws ValidationException {
        Offer offer = new Offer(EXISTING_OFFER);
        offer.setDescription("NEW DESCRIPTION");
        Offer updated = offerService.updateOffer(offer);
        assertNotEquals(updated, EXISTING_OFFER);
    }

    @Test(expected = ValidationException.class)
    public void shouldThrowExceptionWhenTryingToUpdateOfferThatDoesntExist() throws ValidationException {
        Offer offer = new Offer(EXISTING_OFFER);
        offer.setDescription("NEW DESCRIPTION");
        offer.setId(NON_EXISTENT_ID);
        Offer updated = offerService.updateOffer(offer);
    }

    @Test
    public void shouldValidateOfferBeforeUpdatingIt() throws ValidationException {
        Offer offer = new Offer(EXISTING_OFFER);
        offer.setDescription("NEW DESCRIPTION");
        Offer updated = offerService.updateOffer(offer);
        verify(validator, times(1)).validate(any(Offer.class));
    }

    private Offer instantiateOffer(String description, double price, int relatedProductId) {
        Offer offer = new Offer();
        offer.setDescription(description);
        offer.setOfferPrice(price);
        offer.setRelatedProductId(relatedProductId);
        return offer;
    }
}
