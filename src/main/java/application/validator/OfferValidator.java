package application.validator;

import application.domain.Offer;
import application.exception.ValidationException;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class OfferValidator {

    public void validate(Offer offer) throws ValidationException {
        if (offer.getDescription() == null || offer.getDescription().isEmpty()) {
            throw new ValidationException("Offers should have a shopper friendly description.");
        }
        if (offer.getRelatedProductId() <= 0) {
            throw new ValidationException("Offers must have a related product ID with a value greater than 0, to show which product the offer is for.");
        }
        String priceStr = String.valueOf(offer.getOfferPrice());
        BigDecimal price = new BigDecimal(priceStr);
        if (price.scale() > 2) {
            throw new ValidationException("The price of an offer should not go to more than 2 decimal places.");
        }
    }
}
