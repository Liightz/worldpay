package application.controller;

import application.domain.Offer;
import application.exception.ValidationException;
import application.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController(value = "/offer")
public class OfferController {

    private OfferService offerService;

    @Autowired
    public OfferController(OfferService offerService)  {
        this.offerService = offerService;
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Offer getOffer(@RequestParam int id) throws ValidationException {
        return offerService.getOffer(id);
    }

    @RequestMapping(path = "/offer/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Offer> getAllOffers() {
        return offerService.getAllOffers();
    }

    @RequestMapping(method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteOffer(@RequestParam int id) throws ValidationException {
        offerService.deleteOffer(id);
    }

    @RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Offer updateOffer(@RequestBody Offer offer) throws ValidationException {
        return offerService.updateOffer(offer);
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Offer createOffer(@RequestBody Offer offer) throws ValidationException {
        return offerService.addOffer(offer);
    }
}
