package application.domain;

public class Offer {
    private Integer id;
    private String description;
    private double offerPrice;
    private int relatedProductId;

    public Offer() {}

    public Offer(Offer offer) {
        this.id = offer.id;
        this.description = offer.getDescription();
        this.offerPrice = offer.getOfferPrice();
        this.relatedProductId = offer.relatedProductId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(double offerPrice) {
        this.offerPrice = offerPrice;
    }

    public int getRelatedProductId() {
        return relatedProductId;
    }

    public void setRelatedProductId(int relatedProductId) {
        this.relatedProductId = relatedProductId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Offer offer = (Offer) o;

        if (Double.compare(offer.offerPrice, offerPrice) != 0) return false;
        if (relatedProductId != offer.relatedProductId) return false;
        if (id != null ? !id.equals(offer.id) : offer.id != null) return false;
        return description != null ? description.equals(offer.description) : offer.description == null;
    }
}
