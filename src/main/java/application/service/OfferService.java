package application.service;

import application.domain.Offer;
import application.exception.ValidationException;
import application.validator.OfferValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class OfferService {

    private OfferValidator offerValidator;
    private Map<Integer, Offer> offers;
    private AtomicInteger nextId;

    @Autowired
    public OfferService(OfferValidator offerValidator) {
        this.offerValidator = offerValidator;
        this.offers = new ConcurrentHashMap<>();
        this.nextId = new AtomicInteger(1);
    }

    public Offer getOffer(int offerId) throws ValidationException {
        confirmOfferExistsWithId(offerId);
        return offers.get(offerId);
    }

    public List<Offer> getAllOffers() {
        return new ArrayList<>(offers.values());
    }

    public Offer addOffer(Offer offer) throws ValidationException {
        if (offer.getId() != null) {
            throw new ValidationException("New offers should not have an ID");
        }
        Offer saved = new Offer(offer);
        saved.setId(getNextId());
        return saveOffer(saved);
    }

    public void deleteOffer(int offerId) throws ValidationException {
        confirmOfferExistsWithId(offerId);
        offers.remove(offerId);
    }

    public Offer updateOffer(Offer offer) throws ValidationException {
        confirmOfferExistsWithId(offer.getId());
        return saveOffer(offer);
    }

    private Offer saveOffer(Offer offer) throws ValidationException {
        offerValidator.validate(offer);
        offers.put(offer.getId(), offer);
        return offer;
    }

    private void confirmOfferExistsWithId(int id) throws ValidationException {
        if (!offers.containsKey(id)) {
            throw new ValidationException(String.format("No offer exist with Id %d.", id));
        }
    }

    private int getNextId() {
        return nextId.getAndIncrement();
    }
}
